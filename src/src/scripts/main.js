// import Muc from 'views/muc'
// import View from 'views/view'
// import Mitra from 'components/mitra.component'
// import Popup from 'components/popup.component'

// readmoreJS
$('article').readmore({
  collapsedHeight: 150,
  speed: 200
});

// Slider
function slider(){
    var active = $('.active').find('img').attr('src')
    $('.big-slider').attr('src', active)


    $('.list-thumbnail').on('click', function () {
      var x = $(this).find('img').attr('src')

      $('.big-slider').attr('src', x)
      $('.list-thumbnail.active').removeClass('active')
      $(this).addClass('active')

    });
  }
  slider();
